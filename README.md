# Validate Meta-tags

Validador de tags e meta-tags seguindo alguns padrões dentro da ONE

## Intalação
`npm install -g https://bitbucket.org/onedigital/validate-meta-tags.git`


#### Extras
Para instalar o projeto globalmente a partir do repositório clonado, execute:

```
cd pasta-do-projeto
npm install -g
```

## Usando

Para validar as tags apenas rode o comando `validate-tags` dentro da pasta do seu projeto.


Exemplo:
```
cd pasta-do-projeto
validate-tags
```

## Desenvolvimento

Faça o clone do projeto e instale suas dependências, preferencialmente utilizando o [Yarn](https://yarnpkg.com/pt-BR/).

Exemplo:
```
git clone https://<seu-usuario>@bitbucket.org/onedigital/validate-meta-tags.git
yarn install
```

Durante o desenvolvimento, crie uma pasta temp dentro da raíz do projeto e insira alguns arquivos com a extensão .html ou .shtm com o conteúdo que você deseja validar.

Para um melhor desenvolvimento execute `yarn dev` assim todas as mudanças que você fizer nos arquivos `.js` reiniciará o script de validação


## Testes

Este projeto utiliza o Jest para efetuar os testes, para rodá-los execute o comando:

`yarn test`
