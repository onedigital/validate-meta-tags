const { isEmpty } = require('../validations');

const checkAlts = (attribs) => {
  const { alt } = attribs;

  if (isEmpty(alt)) throw new Error('Existe tag img sem o atributo alt');
};

const checkMetaTags = (attribs = {}) => {
  checkAlts(attribs);
};

module.exports = checkMetaTags;
