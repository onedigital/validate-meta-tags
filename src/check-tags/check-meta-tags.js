const { isURL, isEmpty, isMasterBranch, isDomainOfOne } = require('./../validations');

const getTwitterTags = () => ['twitter:card', 'twitter:site', 'twitter:creator', 'twitter:title', 'twitter:description', 'twitter:image'];

const getFacebookTags = () => ['og:site_name', 'og:title', 'og:description', 'og:image', 'og:type', 'og:url'];

const seoTags = () => ['description'];

const allRequiredMetaTags = () => [...getTwitterTags(), ...getFacebookTags(), ...seoTags()];

const checkHMLURL = (url, name) => {
  if (isMasterBranch() && isDomainOfOne(url)) throw new Error(`A meta tag com o nome de ${name} está dentro dominío da ONE`);
  if (!isURL(url)) throw new Error(`A meta tag com o atributo ${name} não é uma URL Válida`);
}

const checkFBTags = ({ property, content }) => {
  if (property.match(/og:image|og:url/)) checkHMLURL(content, property)
};

const checkTwitterTags = ({ name, content }) => {
  if (name.match(/twitter:image/)) checkHMLURL(content, name)
};


const checkAllRequiredMetaTags = (content, tagName) => {
  if (isEmpty(content)) throw new Error(`A meta tag ${tagName} está vazia`);
};

const checkMetaTags = (attribs = {}) => {
  const tagName = attribs.name || attribs.property;

  if (allRequiredMetaTags()
    .some(tag => tag === tagName)) checkAllRequiredMetaTags(attribs.content, tagName);

  if (getFacebookTags().some(fbTag => fbTag === tagName)) {
    checkFBTags(attribs);
  }

  if (getTwitterTags().some(twitterTag => twitterTag === tagName)) {
    checkTwitterTags(attribs);
  }
};

module.exports = checkMetaTags;
