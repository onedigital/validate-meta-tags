const checkMetaTags = require('./check-meta-tags');
const checkImgTags = require('./check-img-tags');

module.exports = {
  checkMetaTags, checkImgTags,
};
