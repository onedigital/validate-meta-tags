const fs = require('fs');

const foldersRegex = new RegExp(/node_modules|bower_components|git/);

function readFile(file) {
  return new Promise((resolve) => {
    fs.readFile(file, (err, data) => resolve(data.toString()));
  });
}

function readDir(dir) {
  return new Promise((resolve) => {
    fs.readdir(dir, (err, files) => { resolve(files); });
  });
}

async function isDirectory(dir, file) {
  const stat = await new Promise((resolve) => {
    fs.stat(dir + file, (err, stats) => resolve(stats));
  });

  return stat.isDirectory();
}

async function getAllHtmlFiles(dir = './', fileList = []) {
  let files = await readDir(dir);

  files = files
    .filter(file => !foldersRegex.test(file));


  await Promise.all(files.map(async (file) => {
    if (await isDirectory(dir, file)) {
      /* eslint-disable */
      fileList = await getAllHtmlFiles(`${dir + file}/`, fileList);
      /* eslint-enable */
    } else if (file.match(/.html|.shtm/)) {
      fileList.push(dir + file);
    }
  }));

  return fileList;
}

module.exports = { readFile, getAllHtmlFiles };
