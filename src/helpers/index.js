const branch = require('git-branch');

const getBranch = () => {
  try {
    return branch.sync();
  } catch (error) {
    return '';
  }
};

module.exports = { getBranch };
