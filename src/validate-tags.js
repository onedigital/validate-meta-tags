const htmlparser = require('htmlparser2');
const { checkMetaTags, checkImgTags } = require('./check-tags');
const { readFile, getAllHtmlFiles } = require('./fs-helpers');
const { getBranch } = require('./helpers');

let actualFile;
let hasErrors = false;

const parser = new htmlparser.Parser({
  onopentag(name, attribs) {
    const checkFunctions = {
      meta: checkMetaTags,
      img: checkImgTags,
    };

    const checkFunction = checkFunctions[name];

    try {
      if (checkFunction) checkFunction(attribs);
    } catch (error) {
      hasErrors = true;
      console.error(`Erro : ${error.message} no arquivo ${actualFile}`);
    }
  },
}, { decodeEntities: true });


async function findAndReadHTMLFiles() {
  const htmlFiles = await getAllHtmlFiles('./');

  await Promise.all(htmlFiles.map(async (htmlPath) => {
    const content = await readFile(htmlPath);
    actualFile = htmlPath;
    parser.parseComplete(content);
  }));
}

const validateTags = async () => {
  console.time('Duração da validação');
  console.log(`Iniciando validação das tags na branch ${getBranch()}`);
  await findAndReadHTMLFiles();
  console.timeEnd('Duração da validação');
  if (hasErrors) process.exit(1);
};


module.exports = validateTags;
