const { getBranch } = require('../helpers');

const isURL = str => /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})/.test(str);

const isDomainOfOne = str => /one.com/gi.test(str);

const isEmpty = str => !str || str === '';

const isMasterBranch = () => getBranch() === 'master';

const isStageBranch = () => getBranch() === 'stage';

module.exports = { isURL, isEmpty, isDomainOfOne, isMasterBranch, isStageBranch };
