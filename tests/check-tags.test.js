const { checkMetaTags, checkImgTags } = require('./../src/check-tags');

describe('checkTags', () => {
  describe('check meta-tags', () => {
    const atribs = {
      fbImg: {
        property: 'og:image',
        content: 'htp://banco-bradesco-hml.one.com.br/cartao-aeternum/facebook.png',
      },
      fbUrl: {
        property: 'og:url',
        content: 'anco-bradesco-hml.one.com.br/cartao-aeternum/',
      },
      twitterImg: {
        name: 'twitter:image',
        content: null,
      },
    };
    it('should throw error when og:image has error', () => {
      let expected;
      try {
        checkMetaTags(atribs.fbImg);
      } catch (error) {
        expected = error;
      }

      expect(expected).toBeInstanceOf(Error);
    });

    it('should throw error when og:url has error', () => {
      let expected;
      try {
        checkMetaTags(atribs.fbUrl);
      } catch (error) {
        expected = error;
      }
      expect(expected).toBeInstanceOf(Error);
    });

    it('should throw error when twitter:image has error', () => {
      let expected;
      try {
        checkMetaTags(atribs.twitterImg);
      } catch (error) {
        expected = error;
      }

      expect(expected).toBeInstanceOf(Error);
    });
  });

  describe('check img tags', () => {
    const imgTag = {
      src: 'img/benefits/assessoria.png',
      alt: 'Campainha de hotel',
    };

    beforeEach(() => {
      imgTag.alt = 'alt';
      imgTag.src = 'img/benefits/assessoria.png';
    });

    it('should throw err when img does not have an alt', () => {
      imgTag.alt = null;
      let expected;
      try {
        checkImgTags(imgTag);
      } catch (error) {
        expected = error;
      }

      expect(expected).toBeInstanceOf(Error);
    });

    it('should throw err when img alt is empty', () => {
      imgTag.alt = '';
      let expected;
      try {
        checkImgTags(imgTag);
      } catch (error) {
        expected = error;
      }

      expect(expected).toBeInstanceOf(Error);
    });
  });
});
