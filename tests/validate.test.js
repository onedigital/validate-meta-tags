const { isURL, isEmpty, isDomainOfOne } = require('./../src/validations');

describe('validations', () => {
  describe('isUrl', () => {
    it('should return true if is a valid url', () => {
      expect(isURL('https://github.com/')).toBeTruthy();
      expect(isURL('http://banco-bradesco-hml.one.com.br/cartao-aeternum/facebook.png')).toBeTruthy();
    });

    it('should return true if is an invalid url', () => {
      expect(isURL('hthub.com/')).toBeFalsy();
      expect(isURL('htts://github.com/')).toBeFalsy();
      expect(isURL('https://githu')).toBeFalsy();
    });
  });

  describe('isDomainOfOne', () => {
    it('should return true if is domain of one', () => {
      expect(isDomainOfOne('one.com.br')).toBeTruthy();
      expect(isDomainOfOne('http://banco-bradesco-hml.one.com.br/cartao-aeternum/facebook.png')).toBeTruthy();
    });
    it('should return false if is not domain of one', () => {
      expect(isDomainOfOne('githhub.com')).toBeFalsy();
      expect(isDomainOfOne('http://banco-bradesco-hml/cartao-aeternum/facebook.png')).toBeFalsy();
    });
  });

  describe('isEmpty', () => {
    it('should return true when atribute is empty', () => {
      const attrib = {};
      expect(isEmpty(attrib.name)).toBeTruthy();
    });

    it('should return true when atribute is equal ""', () => {
      const attrib = { name: '' };
      expect(isEmpty(attrib.name)).toBeTruthy();
    });
  });
});
